.PHONY: test build docker_build

test:
	go vet ./...
	go test ./... -v -cover

build:
	go build -o url-collector cmd/url-collecter/main.go

docker_build:
	docker build -t gogo_apps:0.1 .
