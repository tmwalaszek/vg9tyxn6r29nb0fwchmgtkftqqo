FROM golang:1.17 AS builder

WORKDIR /app

COPY . .
RUN CGO_ENABLED=0 go build -o main cmd/url-collecter/main.go

FROM scratch
WORKDIR /app
COPY --from=builder /app/main .
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

ENTRYPOINT ["./main"]
