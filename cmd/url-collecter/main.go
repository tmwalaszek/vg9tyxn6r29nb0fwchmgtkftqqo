package main

import (
	"log"
	"nasa/pkg/api/nasa"
	"nasa/pkg/handler"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		apiKey = "DEMO_KEY"
	}

	var maxRequests int
	var err error

	maxRequestsEnv := os.Getenv("CONCURRENT_REQUESTS")
	if maxRequestsEnv == "" {
		maxRequests = 5
	} else {
		maxRequests, err = strconv.Atoi(maxRequestsEnv)
		if err != nil {
			log.Fatalf("CONCURRENT_REQUESTS env variable wrong format: %v", err)
		}
	}

	nasaAPI := nasa.NewNasaAPI(apiKey, maxRequests)

	mux := handler.NewHandler(nasaAPI)

	server := &http.Server{
		Addr:         "0.0.0.0:" + port,
		Handler:      mux,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	log.Printf("Server listen on 0.0.0.0:%s\n", port)
	log.Fatal(server.ListenAndServe())

}
