package nasa

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"
)

func TestParallelAPI(t *testing.T) {
	cnt := 0
	lock := &sync.Mutex{}

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadFile("test_data/response.golden")
		if err != nil {
			t.Fatalf("Can't read reponse.golden: %v", err)
		}
		time.Sleep(10 * time.Second)
		lock.Lock()
		cnt++
		lock.Unlock()

		w.WriteHeader(200)
		w.Write([]byte(body))
	}))

	defer srv.Close()

	nasaAPI := NewNasaAPI("DEMO_KEY", 5)
	nasaAPI.apiURL = srv.URL
	go nasaAPI.GetURLS([]string{"1", "2", "3", "4", "5", "6", "7", "8"})

	time.Sleep(11 * time.Second)
	if cnt != 5 {
		t.Errorf("Only 5 workers should be able to make a request but our counter is %d", cnt)
	}
}
func TestNasaAPI(t *testing.T) {
	// Test correct response
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadFile("test_data/response.golden")
		if err != nil {
			t.Fatalf("Can't read reponse.golden: %v", err)
		}

		w.WriteHeader(200)
		w.Write([]byte(body))
	}))

	defer srv.Close()

	// Test error response
	srvErr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadFile("test_data/error.golden")
		if err != nil {
			t.Fatalf("Can't read reponse.golden: %v", err)
		}

		w.WriteHeader(429)
		w.Write([]byte(body))
	}))

	defer srvErr.Close()

	var tt = []struct {
		Name     string
		Srv      *httptest.Server
		Ok       bool
		Response string
	}{
		{"Good results", srv, true, "https://apod.nasa.gov/apod/image/2101/2020_12_16_Kujal_Jizni_Pol_1500px-3.jpg"},
		{"Error results", srvErr, false, "You have exceeded your rate limit. Try again later or contact us at https://api.nasa.gov:443/contact/ for assistance"},
	}

	for _, tc := range tt {
		nasaAPI := NewNasaAPI("DEMO_KEY", 5)
		nasaAPI.apiURL = tc.Srv.URL

		results, errorList := nasaAPI.GetURLS([]string{"2021-01-01"})
		if tc.Ok {
			if len(errorList) > 0 {
				t.Errorf("We should not get any errorList but we have: %v", errorList)
			}

			if len(results) != 1 {
				t.Errorf("We should get 1 results but we have: %v", results)
			}

			if results[0] != tc.Response {
				t.Errorf("Wrong result. We have %s but we wanted %s", results[0], tc.Response)
			}
		} else {
			if len(results) > 0 {
				t.Errorf("We should not get any results but we have: %v", results)
			}

			if len(errorList) != 1 {
				t.Errorf("We should get 1 error but we have: %v", errorList)
			}

			if errorList[0].Error() != tc.Response {
				t.Errorf("We should get %s but we have %s", tc.Response, errorList[0].Error())
			}
		}
	}
}
