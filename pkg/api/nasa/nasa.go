// Package nasa is responsible for interacting with NASA API
package nasa

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"sync"
)

type NasaAPI struct {
	apiKey string
	apiURL string

	maxRequests int

	c *http.Client
}

type nasaAPIResponse struct {
	Url string `json:"url"`
}

type errorMsg struct {
	ErrorMsg string `json:"msg,omitempty"`
	Error    struct {
		Code    string `json:"code,omitempty"`
		Message string `json:"message,omitempty"`
	} `json:"error,omitempty"`
}

func NewNasaAPI(apiKey string, maxRequests int) *NasaAPI {
	transport := &http.Transport{
		MaxConnsPerHost:     maxRequests,
		MaxIdleConnsPerHost: 0,
		MaxIdleConns:        0,
	}

	return &NasaAPI{
		apiKey: apiKey,
		apiURL: "https://api.nasa.gov/planetary/apod",
		c: &http.Client{
			Transport: transport,
		},
		maxRequests: maxRequests,
	}
}

// GetURLS takes dates slice and makes len(dates) goroutines
// Each goroutine will make a call and append result to urls or error to errors
// In multiple requeests we can have multiple errors so it is up to the user to decide what to do
func (n *NasaAPI) GetURLS(dates []string) ([]string, []error) {
	urls := make([]string, 0)
	errorList := make([]error, 0)
	lock := &sync.Mutex{}

	wg := &sync.WaitGroup{}

	for _, date := range dates {
		wg.Add(1)
		go func(date string) {
			url, err := n.getURL(date)
			lock.Lock()
			defer lock.Unlock()

			if err != nil {
				errorList = append(errorList, err)
			} else {
				urls = append(urls, url)
			}

			wg.Done()
		}(date)
	}

	wg.Wait()

	return urls, errorList
}

func (n *NasaAPI) getURL(date string) (string, error) {
	var nasaResponse *nasaAPIResponse
	var errMsg *errorMsg

	u, err := url.Parse(n.apiURL)
	if err != nil {
		return "", err
	}

	params := url.Values{}
	params.Add("api_key", n.apiKey)
	params.Add("date", date)
	u.RawQuery = params.Encode()

	res, err := n.c.Get(u.String())
	if err != nil {
		return "", err
	}

	defer res.Body.Close()

	var dec *json.Decoder
	if res.StatusCode != 200 {
		dec = json.NewDecoder(res.Body)
		err = dec.Decode(&errMsg)
		if err != nil {
			return "", err
		}

		if errMsg.ErrorMsg != "" {
			return "", errors.New(errMsg.ErrorMsg)
		} else if errMsg.Error.Message != "" {
			return "", errors.New(errMsg.Error.Message)
		} else {
			return "", errors.New("unknwon error message format from NASA API")
		}
	}

	dec = json.NewDecoder(res.Body)
	err = dec.Decode(&nasaResponse)
	if err != nil {
		return "", err
	}

	return nasaResponse.Url, nil
}
