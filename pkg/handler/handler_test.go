package handler

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
)

type MockAPI struct {
	mock.Mock
}

func (m *MockAPI) GetURLS(dates []string) ([]string, []error) {
	args := m.Called(dates)

	return args.Get(0).([]string), args.Get(1).([]error)
}

func TestUniqueErrors(t *testing.T) {
	var tt = []struct {
		Name   string
		Input  []error
		Output []error
	}{
		{"Test one", []error{errors.New("test1"), errors.New("test2")}, []error{errors.New("test1"), errors.New("test2")}},
		{"Test two", []error{errors.New("test1"), errors.New("test1")}, []error{errors.New("test1")}},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(*testing.T) {
			uniq := uniqErrors(tc.Input)

			if !reflect.DeepEqual(uniq, tc.Output) {
				t.Errorf("Expected %v but got %v", tc.Output, uniq)
			}
		})
	}
}

func TestPictureHandler(t *testing.T) {
	mockApi := &MockAPI{}
	var errorList []error
	mockApi.On("GetURLS", []string{"2021-08-01", "2021-08-02"}).Return([]string{"ok", "ok"}, errorList)
	mockApi.On("GetURLS", []string{"2022-08-01", "2021-08-02"}).Return([]string{}, errorList)

	newHandler := NewHandler(mockApi)

	srv := httptest.NewServer(newHandler)
	defer srv.Close()

	var tt = []struct {
		Name     string
		Query    string
		Response string // Path to golden file
	}{
		{"Good response", "/pictures?start_date=2021-08-01&end_date=2021-08-02", "test_data/ok.golden"},
		{"Bad arguments", "/pictures?start_date=2022-08-01&end_date=2021-08-02", "test_data/start_greater.golden"},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			expected, err := ioutil.ReadFile(tc.Response)
			if err != nil {
				t.Fatalf("Can't open golden file: %v", tc.Response)
			}

			resp, err := http.Get(srv.URL + tc.Query)
			if err != nil {
				t.Fatalf("Request failed: %v", err)
			}

			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Can't read req body: %v", err)
			}

			if !reflect.DeepEqual(expected, body[:len(body)-1]) {
				t.Errorf("Wrong response, expected %s but have %s", string(expected), string(body[:len(body)-1]))
			}
		})
	}
}
