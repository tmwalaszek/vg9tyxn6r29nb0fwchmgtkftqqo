package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

var (
	ErrStartAfterEnd = errors.New("start date is greater then end date")
)

type response struct {
	ErrorMsg string      `json:"error,omitempty"`
	Urls     interface{} `json:"urls,omitempty"`
}

type SpaceAPI interface {
	GetURLS([]string) ([]string, []error)
}

type handler struct {
	spaceAPI SpaceAPI
}

func encodeError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(response{ErrorMsg: err.Error()})
}

func (h *handler) pictures(w http.ResponseWriter, r *http.Request) {
	vars := r.URL.Query()

	var startDate, endDate string
	var start, end time.Time

	var err error

	w.Header().Set("Content-Type", "application/json")

	startDate = vars.Get("start_date")
	if startDate == "" {
		start = time.Now()
	} else {
		start, err = time.Parse("2006-01-02", startDate)
		if err != nil {
			encodeError(w, err)
			return
		}
	}

	endDate = vars.Get("end_date")
	if endDate == "" {
		end = time.Now()
	} else {
		end, err = time.Parse("2006-01-02", endDate)
		if err != nil {
			encodeError(w, err)
			return
		}
	}

	if start.After(end) {
		encodeError(w, ErrStartAfterEnd)
		return
	}

	dates := make([]string, 0)
	for start.Before(end) || start.Equal(end) {
		formatDate := start.Format("2006-01-02")

		dates = append(dates, formatDate)
		start = start.Add(time.Hour * 24)
	}

	urls, errorList := h.spaceAPI.GetURLS(dates)
	if len(errorList) > 0 {
		uniqErrors := uniqErrors(errorList)
		encodeError(w, fmt.Errorf("errorList from NASA API: %v", uniqErrors))
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response{Urls: urls})
}

func uniqErrors(errors []error) []error {
	keys := make(map[string]bool)
	uniqErrors := make([]error, 0)

	for _, err := range errors {
		if _, value := keys[err.Error()]; !value {
			keys[err.Error()] = true
			uniqErrors = append(uniqErrors, err)
		}
	}

	return uniqErrors
}

func NewHandler(spaceAPI SpaceAPI) http.Handler {
	//serverMux := http.NewServeMux()
	mux := mux.NewRouter()
	h := handler{
		spaceAPI: spaceAPI,
	}

	mux.HandleFunc("/pictures", h.pictures)
	loggedRouter := handlers.LoggingHandler(os.Stdout, mux)
	//serverMux.Handle("/", mux)

	return loggedRouter
}
